print "Digite o primeiro número: "
num1 = gets.chomp.to_i

print "Digite o segundo número: "
num2 = gets.chomp.to_i

soma = num1 + num2
subtracao = num1 - num2
multiplicacao = num1 * num2
divisao = num1 / num2

puts "A soma dos números é #{soma}, a subtração é #{subtracao}, a multiplicação é #{multiplicacao} e a divisão é #{divisao}!"